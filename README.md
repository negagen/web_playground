# Playground

This is a playground for testing and learning new things, while trying to keep it as fast and small as possible, so it can be used as a reference for future projects.

## Things to play with

- [x] [PWA (Progressive Web App)](https://web.dev/progressive-web-apps/)
- [ ] [BLE (Bluetooth Low Energy)](https://web.dev/bluetooth/)
- [ ] [OAuth 2.0](https://oauth.net/2/)

## Maybe?

- [ ] Add self-signed certificate for HTTPS, and test it on a local network, it works on pythonanywhere, but not on localhost.
