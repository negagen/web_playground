# We create environment for our project, install uWSGI and copy our project files to the container
# We use miniconda3 as base image

FROM continuumio/miniconda3 as base

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install environment dependencies
RUN conda env create -f environment.yml

# Make port 5000 available to the world outside this container
EXPOSE 5000

# Entrypoint for the container should be the conda environment
ENTRYPOINT ["/bin/bash", "/app/entrypoint.sh"]