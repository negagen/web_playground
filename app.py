from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route('/')
def homepage():
    return render_template('index.html')

# Favicon is in the static folder
@app.route('/favicon.ico')
def favicon():
    return app.send_static_file('favicon.ico')

# We need the manifest on the root path
@app.route('/manifest.json')
def manifest():
    return app.send_static_file('manifest.json')